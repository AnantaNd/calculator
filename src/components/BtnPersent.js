import {ACT} from "../App";


export default function BtnPersent({dispatch, operation}){
  return (
    <button className="btnUp"
      onClick={() => 
        dispatch({ type: ACT.operation, payload: {operation}})}
      >{operation}
    </button>
  )
}